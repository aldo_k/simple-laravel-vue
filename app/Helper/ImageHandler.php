<?php

namespace App\Helper;

use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Enc;

class ImageHandler 
{
	
	public static function upload($image,$filename,$path){
		try
		{
			$enc = new Enc();

			$allowed = ['txt','gif','png','jpg','jpeg'];
			$detect = pathinfo($filename);
			if (in_array(strtolower($detect['extension']), $allowed))
			{
				$extension = $detect['extension'];

				$imageInfo = explode(";base64,", $image);
				$image = @base64_decode(@str_replace(' ', '+', $imageInfo[1]));
			} else {
				$extension = "tmp";
			}
			$name = time().'.'.$extension;

			if(Storage::disk('public_uploads')->put("/products/small/".$name, $image))
			{

				$imageEncrypt = $enc->encode($image, $name);

				Storage::disk('public_uploads')->put("/products/small/".$name.".hasilEnkripsi", base64_decode($imageEncrypt));

				return $name;
			}             
		} catch(\Exception $e) {
			// dd($e);
			return null;
		}     

		return $name;
	}	
}