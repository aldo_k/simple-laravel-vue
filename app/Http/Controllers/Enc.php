<?php
namespace App\Http\Controllers;

class Enc {
  public $NilaiIndexBase64 = "vwxyz0123456789+/ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstu";
	
  public function encode($Plaintext, $Key){

    try {
      // Memeriksa Chipertext Tidak Kosong
      if($Plaintext == null || $Key == null)
      {
        // Pesan Bahwa Chipertext Kosong
        throw new \Exception("INPUT IS NULL");
      }
    } catch (\Exception $e) {
      return (isset($e->xdebug_message)? $e->xdebug_message : $e->getMessage());
    }
    
    $NilaiIndexBase64 = $this->NilaiIndexBase64;

    $PanjangPlaintext = strlen($Plaintext);
    $Plaintext = str_split($Plaintext);

    $PanjangKey = (strlen($Key) < $PanjangPlaintext) ? strlen($Key) : $PanjangPlaintext;

    $Key = str_split($Key);
    $IndexKey = array();
    for ($LoopKey = 0; $LoopKey < $PanjangKey; $LoopKey += 3)  {
      // Pangkas 6 Bit Pertama Jadi Nilai Decimal
      $VariablePenampungKey = (ord($Key[$LoopKey]) & 252) >> 2;

      // Index Key Ke $LoopKey
      $IndexKey[] = $VariablePenampungKey;

      // Pangkas 6 Bit Kedua
      $VariablePenampungKey = (ord($Key[$LoopKey]) & 3) << 4;

      if ($LoopKey + 1 < $PanjangKey) {

        $VariablePenampungKey |= (ord($Key[$LoopKey + 1]) & 240) >> 4;

        // Index Key Ke $LoopKey+1
        $IndexKey[] = $VariablePenampungKey;

        // Pangkas 6 Bit Ketiga
        $VariablePenampungKey = (ord($Key[$LoopKey + 1]) & 15) << 2;

        if ($LoopKey + 2 < $PanjangKey)  {

          $VariablePenampungKey |= (ord($Key[$LoopKey + 2]) & 192) >> 6;

          // Index Key Ke $LoopKey+2
          $IndexKey[] = $VariablePenampungKey;

          // Pangkas Index Ke Empat
          $VariablePenampungKey = ord($Key[$LoopKey + 2]) & 63;

          // Index Key Ke $LoopKey+3
          $IndexKey[] = $VariablePenampungKey;

        } else  {

          // Index Key Ke $LoopKey+2
          $IndexKey[] = $VariablePenampungKey;
        }
      } else {

        // Index Key Ke $LoopKey+1
        $IndexKey[] = $VariablePenampungKey;
      }
    }

    $PanjangKey = count($IndexKey);
    
    // ((jumlah / panjang key)+47)%64
    // $SecretAdding = ((int)(array_sum($IndexKey) / $PanjangKey)+47)%64;
    // ((jumlah)+47)%64
    // $SecretAdding = ((int)(array_sum($IndexKey))+47)%64;
    // Ditambah +47
    $SecretAdding = 47;

    $HasilEncode = '';
    $VariablePenampungPlaintext = '';
    $VariablePenampungKey = '';
    $Hitung = 1;

    $loopKeyXOR = 0;

    // Perulangan Menghitung Plaintext Per Tiga Karakter Untuk Dipisah Per 6 Bit
    for ($i = 0; $i < $PanjangPlaintext; $i += 3)  {
      // Pangkas 6 Bit Pertama Jadi Nilai Decimal
      $VariablePenampungPlaintext = (ord($Plaintext[$i]) & 252) >> 2;

      // Mencari Nilai Index Ke-1 Key dan Melakukan ROT47 Pada Key Tersebut
      // $HasilKey[] = $VariablePenampungKey = (ord($Key[$i % $PanjangKey]) & 252) >> 2;

      // Melakukan XOR Antara Index Ke-1 Plaintext Dengan Index Ke-1 Key Yg Telah Di ROT47
      // $VariablePenampungPlaintext = (($VariablePenampungPlaintext+47)%64) ^ $VariablePenampungKey;
      $VariablePenampungPlaintext = (($VariablePenampungPlaintext+$SecretAdding)%64) ^ $IndexKey[($loopKeyXOR++) % $PanjangKey];

      // Menampung Data Dalam Bentu Decimal, Hexa, Dan Binary Pada Index Ke 1
      $DataIndexDecimal[$Hitung]['1'] = $VariablePenampungPlaintext;
      $DataIndexHexa[$Hitung]['1']    = sprintf("%02s", dechex($VariablePenampungPlaintext));
      $DataIndexBin[$Hitung]['1']     = sprintf("%06s", decbin($VariablePenampungPlaintext));

      // Hasil Pangkas 6 Bit Pertama
      $HasilEncode .= ($NilaiIndexBase64[$VariablePenampungPlaintext]);
      
      // Pangkas 6 Bit Kedua
      $VariablePenampungPlaintext = (ord($Plaintext[$i]) & 3) << 4;
      // $VariablePenampungKey       = (ord($Key[$i % $PanjangKey]) & 3) << 4;
      if ($i + 1 < $PanjangPlaintext) {

        $VariablePenampungPlaintext |= (ord($Plaintext[$i + 1]) & 240) >> 4;

        // Mencari Nilai Index Ke-2 Key dan Melakukan ROT47 Pada Key Tersebut
        // $HasilKey[] = $VariablePenampungKey = (ord($Key[($i+1) % $PanjangKey]) & 240) >> 4;

        // Melakukan XOR Antara Index Ke-2 Plaintext Dengan Index Ke-2 Key Yg Telah Di ROT47
        // $VariablePenampungPlaintext = (($VariablePenampungPlaintext+47)%64) ^ $VariablePenampungKey;
        $VariablePenampungPlaintext = (($VariablePenampungPlaintext+$SecretAdding)%64) ^ $IndexKey[($loopKeyXOR++) % $PanjangKey];
        
        // Menampung Data Dalam Bentu Decimal, Hexa, Dan Binary Pada Index Ke 2
        $DataIndexDecimal[$Hitung]['2'] = $VariablePenampungPlaintext;
        $DataIndexHexa[$Hitung]['2']    = sprintf("%02s", dechex($VariablePenampungPlaintext));
        $DataIndexBin[$Hitung]['2']     = sprintf("%06s", decbin($VariablePenampungPlaintext));

        // Hasil 6 Bit Kedua Jika Plaintext Kelipatan Lebih Dari 2
        $HasilEncode .= ($NilaiIndexBase64[$VariablePenampungPlaintext]);

        // Pangkas 6 Bit Ketiga
        $VariablePenampungPlaintext = (ord($Plaintext[$i + 1]) & 15) << 2;
        // $VariablePenampungKey       = (ord($Key[( $i + 1 ) % $PanjangKey]) & 15) << 2;

        if ($i + 2 < $PanjangPlaintext)  {

          $VariablePenampungPlaintext |= (ord($Plaintext[$i + 2]) & 192) >> 6;

          // Mencari Nilai Index Ke-3 Key dan Melakukan ROT47 Pada Key Tersebut
          // $HasilKey[] = $VariablePenampungKey = (ord($Key[($i+2) % $PanjangKey]) & 192) >> 6;

          // Melakukan XOR Antara Index Ke-3 Plaintext Dengan Index Ke-3 Key Yg Telah Di ROT47
          // $VariablePenampungPlaintext = (($VariablePenampungPlaintext+47)%64) ^ $VariablePenampungKey;
          $VariablePenampungPlaintext = (($VariablePenampungPlaintext+$SecretAdding)%64) ^ $IndexKey[($loopKeyXOR++) % $PanjangKey];;

          // Menampung Data Dalam Bentu Decimal, Hexa, Dan Binary Pada Index Ke 3
          $DataIndexDecimal[$Hitung]['3'] = $VariablePenampungPlaintext;
          $DataIndexHexa[$Hitung]['3']    = sprintf("%02s", dechex($VariablePenampungPlaintext));
          $DataIndexBin[$Hitung]['3']     = sprintf("%06s", decbin($VariablePenampungPlaintext));

          // Hasil 6 Bit Ketiga Jika Plaintext Kelipatan Lebih Dari 3
          $HasilEncode .= ($NilaiIndexBase64[$VariablePenampungPlaintext]);
          
          // Pangkas Index Ke Empat
          $VariablePenampungPlaintext = ord($Plaintext[$i + 2]) & 63;

          // Mencari Nilai Index Ke-4 Key dan Melakukan ROT47 Pada Key Tersebut
          // $HasilKey[] = $VariablePenampungKey = (ord($Key[($i+2) % $PanjangKey]) & 63);

          // Melakukan XOR Antara Index Ke-4 Plaintext Dengan Index Ke-4 Key Yg Telah Di ROT47
          // $VariablePenampungPlaintext = (($VariablePenampungPlaintext+47)%64) ^ $VariablePenampungKey;
          // $VariablePenampungPlaintext = (($VariablePenampungPlaintext+47)%64) ^ $IndexKey[($i+2) % $PanjangKey];;
          $VariablePenampungPlaintext = (($VariablePenampungPlaintext+$SecretAdding)%64) ^ $IndexKey[($loopKeyXOR++) % $PanjangKey];;

          // Menampung Data Dalam Bentu Decimal, Hexa, Dan Binary Pada Index Ke 4
          $DataIndexDecimal[$Hitung]['4'] = $VariablePenampungPlaintext;
          $DataIndexHexa[$Hitung]['4']    = sprintf("%02s", dechex($VariablePenampungPlaintext));
          $DataIndexBin[$Hitung]['4']     = sprintf("%06s", decbin($VariablePenampungPlaintext));
          
          // Hasil Index Ke Empat
          $HasilEncode .= ($NilaiIndexBase64[$VariablePenampungPlaintext]);

        } else  {

          // Mencari Nilai Index Ke-3 Key dan Melakukan ROT47 Pada Key Tersebut
          // $HasilKey[] = $VariablePenampungKey;

          // Melakukan XOR Antara Index Ke-3 Plaintext Dengan Index Ke-3 Key Yg Telah Di ROT47
          // $VariablePenampungPlaintext = (($VariablePenampungPlaintext+47)%64) ^ $VariablePenampungKey;
          $VariablePenampungPlaintext = (($VariablePenampungPlaintext+$SecretAdding)%64) ^ $IndexKey[($loopKeyXOR++) % $PanjangKey];

          // Menampung Data Dalam Bentu Decimal, Hexa, Dan Binary Pada Index Ke 3
          $DataIndexDecimal[$Hitung]['3'] = $VariablePenampungPlaintext;
          $DataIndexHexa[$Hitung]['3']    = sprintf("%02s", dechex($VariablePenampungPlaintext));
          $DataIndexBin[$Hitung]['3']     = sprintf("%06s", decbin($VariablePenampungPlaintext));

          // Hasil Index Ke Tiga Jika Plaintext Kelipatan Kurang Dari 3
          $HasilEncode .= ($NilaiIndexBase64[$VariablePenampungPlaintext]);
          $HasilEncode .= ('=');
        }
      } else{
        // Mencari Nilai Index Ke-2 Key dan Melakukan ROT47 Pada Key Tersebut
        // $HasilKey[] = $VariablePenampungKey;

        // Melakukan XOR Antara Index Ke-2 Plaintext Dengan Index Ke-2 Key Yg Telah Di ROT47
        // $VariablePenampungPlaintext = (($VariablePenampungPlaintext+47)%64) ^ $VariablePenampungKey;
        $VariablePenampungPlaintext = (($VariablePenampungPlaintext+$SecretAdding)%64) ^ $IndexKey[($loopKeyXOR++) % $PanjangKey];;

        // Menampung Data Dalam Bentu Decimal, Hexa, Dan Binary Pada Index Ke 2
        $DataIndexDecimal[$Hitung]['2'] = $VariablePenampungPlaintext;
        $DataIndexHexa[$Hitung]['2']    = sprintf("%02s", dechex($VariablePenampungPlaintext));
        $DataIndexBin[$Hitung]['2']     = sprintf("%06s", decbin($VariablePenampungPlaintext));

        // Hasil 6 Bit Kedua Jika Plaintext Hanya 1 Karakter
        $HasilEncode .= ($NilaiIndexBase64[$VariablePenampungPlaintext]);
        $HasilEncode .= ("==");
      }
      $Hitung++;
    }
    return $HasilEncode;
    // return [
    //   "HasilEncode" => base64_encode($HasilEncode),
    //   "HasilKey" => $IndexKey,
    //   "Index" => [
    //     "Decimal" => $DataIndexDecimal,
    //     "Hexa" => $DataIndexHexa,
    //     "Binary" => $DataIndexBin,
    //   ]
    // ];
  }

  public function decode($Chipertext, $Key){
    $NilaiIndexBase64 = $this->NilaiIndexBase64;

    try {
      // Memeriksa Chipertext Tidak Kosong
      if($Chipertext == null || $Key == null)
      {
        // Pesan Bahwa Chipertext Kosong
        throw new \Exception("INPUT IS NULL");
      }
    } catch (\Exception $e) {
      return (isset($e->xdebug_message)? $e->xdebug_message : $e->getMessage());
    }

    try {
      // Memeriksa Format Chipertext
      if (strlen($Chipertext) % 4 != 0)
      {
        // Pesan Bahwa Format Chipertext Tidak Sesuai
        throw new \Exception("INVALID BASE64 STRING");
      }
    } catch (\Exception $e) {
      return (isset($e->xdebug_message)? $e->xdebug_message : $e->getMessage());
    }

    $HitungPanjangPlaintext = strlen(base64_decode($Chipertext));

    $PlaintextChars = str_split($Chipertext);

    $PanjangKey = (strlen($Key) < $HitungPanjangPlaintext) ? strlen($Key) : $HitungPanjangPlaintext;
    $Key = str_split($Key);

    $IndexKey = array();
    for ($LoopKey = 0; $LoopKey < $PanjangKey; $LoopKey += 3)  {
      // Pangkas 6 Bit Pertama Jadi Nilai Decimal
      $VariablePenampungKey = (ord($Key[$LoopKey]) & 252) >> 2;

      // Index Key Ke $LoopKey
      $IndexKey[] = $VariablePenampungKey;

      // Pangkas 6 Bit Kedua
      $VariablePenampungKey = (ord($Key[$LoopKey]) & 3) << 4;

      if ($LoopKey + 1 < $PanjangKey) {

        $VariablePenampungKey |= (ord($Key[$LoopKey + 1]) & 240) >> 4;

        // Index Key Ke $LoopKey+1
        $IndexKey[] = $VariablePenampungKey;

        // Pangkas 6 Bit Ketiga
        $VariablePenampungKey = (ord($Key[$LoopKey + 1]) & 15) << 2;

        if ($LoopKey + 2 < $PanjangKey)  {

          $VariablePenampungKey |= (ord($Key[$LoopKey + 2]) & 192) >> 6;

          // Index Key Ke $LoopKey+2
          $IndexKey[] = $VariablePenampungKey;

          // Pangkas Index Ke Empat
          $VariablePenampungKey = ord($Key[$LoopKey + 2]) & 63;

          // Index Key Ke $LoopKey+3
          $IndexKey[] = $VariablePenampungKey;

        } else  {

          // Index Key Ke $LoopKey+2
          $IndexKey[] = $VariablePenampungKey;
        }
      } else {

        // Index Key Ke $LoopKey+1
        $IndexKey[] = $VariablePenampungKey;
      }
    }

    $PanjangKey = count($IndexKey);

    // ((jumlah / panjang key)+47)%64
    // $SecretAdding = ((int)(array_sum($IndexKey) / $PanjangKey)+47)%64;
    // ((jumlah)+47)%64
    // $SecretAdding = ((int)(array_sum($IndexKey))+47)%64;
    // Ditambah +47
    $SecretAdding = 47;

    $j = 0;

    $Hitung = 1;

    $VariablePenampungPlaintext = array();

    for ($i = 0; $i < count($PlaintextChars); $i += 4) {

      $VariablePenampungPlaintext[0] = (((strpos($NilaiIndexBase64,$PlaintextChars[$i]) ^ $IndexKey[($i % $PanjangKey)])+64)-$SecretAdding)%64;
      $VariablePenampungPlaintext[1] = (((strpos($NilaiIndexBase64,$PlaintextChars[$i+1]) ^ $IndexKey[($i+1) % $PanjangKey])+64)-$SecretAdding)%64;
      $VariablePenampungPlaintext[2] = (((strpos($NilaiIndexBase64,$PlaintextChars[$i+2]) ^ $IndexKey[($i+2) % $PanjangKey])+64)-$SecretAdding)%64;
      $VariablePenampungPlaintext[3] = (((strpos($NilaiIndexBase64,$PlaintextChars[$i+3]) ^ $IndexKey[($i+3) % $PanjangKey])+64)-$SecretAdding)%64;

      $PlaintextChars[$i]!="=" ? $VariablePenampungPlaintext[0] = $VariablePenampungPlaintext[0] : $VariablePenampungPlaintext[0] = 999;
      $PlaintextChars[$i+1]!="=" ? $VariablePenampungPlaintext[1] = $VariablePenampungPlaintext[1] : $VariablePenampungPlaintext[1] = 999;
      $PlaintextChars[$i+2]!="=" ? $VariablePenampungPlaintext[2] = $VariablePenampungPlaintext[2] : $VariablePenampungPlaintext[2] = 999;
      $PlaintextChars[$i+3]!="=" ? $VariablePenampungPlaintext[3] = $VariablePenampungPlaintext[3] : $VariablePenampungPlaintext[3] = 999;

      $DataDecode = $VariablePenampungDecode[$j++] = (($VariablePenampungPlaintext[0] << 2) | ($VariablePenampungPlaintext[1] >> 4));

      $DataIndexDecimal[$Hitung]['1'] = $DataDecode % 256;
      $DataIndexHexa[$Hitung]['1']    = sprintf("%02s", dechex($VariablePenampungDecode[($j-1)] % 256));
      $DataIndexBin[$Hitung]['1']     = sprintf("%08s", decbin($VariablePenampungDecode[($j-1)] % 256));

      if ($VariablePenampungPlaintext[2] < 64)
      {

        $DataDecode = $VariablePenampungDecode[$j++] = (($VariablePenampungPlaintext[1] << 4) | ($VariablePenampungPlaintext[2] >> 2));

        $DataIndexDecimal[$Hitung]['2'] = $DataDecode % 256;
        $DataIndexHexa[$Hitung]['2']    = sprintf("%02s", dechex($VariablePenampungDecode[($j-1)] % 256));
        $DataIndexBin[$Hitung]['2']     = sprintf("%08s", decbin($VariablePenampungDecode[($j-1)] % 256));

        if ($VariablePenampungPlaintext[3] < 64)  {

          $DataDecode = $VariablePenampungDecode[$j++] = (($VariablePenampungPlaintext[2] << 6) | $VariablePenampungPlaintext[3]);

          $DataIndexDecimal[$Hitung]['3'] = $DataDecode % 256;
          $DataIndexHexa[$Hitung]['3']    = sprintf("%02s", dechex($VariablePenampungDecode[($j-1)] % 256));
          $DataIndexBin[$Hitung]['3']     = sprintf("%08s", decbin($VariablePenampungDecode[($j-1)] % 256));

        }
      }
      $Hitung++;
    }
    $HasilDecode = '';
    for($i=0;$i<$HitungPanjangPlaintext;$i++)
    {
      $HasilDecode .= chr($VariablePenampungDecode[$i]);

    }
    return $HasilDecode;
    // return [
    //   "HasilDecode" => base64_encode($HasilDecode),
    //   "Index" => [
    //     "Decimal" => $DataIndexDecimal,
    //     "Hexa" => $DataIndexHexa,
    //     "Binary" => $DataIndexBin,
    //   ],
    //   "IndexKey" => $IndexKey,
    //   "IndexAwal" => $Save,
    //   "Penampung" => $VariablePenampungDecode,
    //   "PanjangKey" => $HitungPanjangPlaintext,
    // ];
  }

  public function encode_debug($Plaintext, $Key){

    try {
      // Memeriksa Chipertext Tidak Kosong
      if($Plaintext == null || $Key == null)
      {
        // Pesan Bahwa Chipertext Kosong
        throw new \Exception("INPUT IS NULL");
      }
    } catch (\Exception $e) {
      return (isset($e->xdebug_message)? $e->xdebug_message : $e->getMessage());
    }
    
    $NilaiIndexBase64 = $this->NilaiIndexBase64;

    $PanjangPlaintext = strlen($Plaintext);
    $Plaintext = str_split($Plaintext);

    $PanjangKey = (strlen($Key) < $PanjangPlaintext) ? strlen($Key) : $PanjangPlaintext;

    $Key = str_split($Key);
    $IndexKey = array();
    for ($LoopKey = 0; $LoopKey < $PanjangKey; $LoopKey += 3)  {
      // Pangkas 6 Bit Pertama Jadi Nilai Decimal
      $VariablePenampungKey = (ord($Key[$LoopKey]) & 252) >> 2;

      // Index Key Ke $LoopKey
      $IndexKey[] = $VariablePenampungKey;

      // Pangkas 6 Bit Kedua
      $VariablePenampungKey = (ord($Key[$LoopKey]) & 3) << 4;

      if ($LoopKey + 1 < $PanjangKey) {

        $VariablePenampungKey |= (ord($Key[$LoopKey + 1]) & 240) >> 4;

        // Index Key Ke $LoopKey+1
        $IndexKey[] = $VariablePenampungKey;

        // Pangkas 6 Bit Ketiga
        $VariablePenampungKey = (ord($Key[$LoopKey + 1]) & 15) << 2;

        if ($LoopKey + 2 < $PanjangKey)  {

          $VariablePenampungKey |= (ord($Key[$LoopKey + 2]) & 192) >> 6;

          // Index Key Ke $LoopKey+2
          $IndexKey[] = $VariablePenampungKey;

          // Pangkas Index Ke Empat
          $VariablePenampungKey = ord($Key[$LoopKey + 2]) & 63;

          // Index Key Ke $LoopKey+3
          $IndexKey[] = $VariablePenampungKey;

        } else  {

          // Index Key Ke $LoopKey+2
          $IndexKey[] = $VariablePenampungKey;
        }
      } else {

        // Index Key Ke $LoopKey+1
        $IndexKey[] = $VariablePenampungKey;
      }
    }

    $PanjangKey = count($IndexKey);
    
    // ((jumlah / panjang key)+47)%64
    // $SecretAdding = ((int)(array_sum($IndexKey) / $PanjangKey)+47)%64;
    // ((jumlah)+47)%64
    // $SecretAdding = ((int)(array_sum($IndexKey))+47)%64;
    // Ditambah +47
    $SecretAdding = 47;

    $HasilEncode = '';
    $VariablePenampungPlaintext = '';
    $VariablePenampungKey = '';
    $Hitung = 1;

    $loopKeyXOR = 0;

    // Perulangan Menghitung Plaintext Per Tiga Karakter Untuk Dipisah Per 6 Bit
    for ($i = 0; $i < $PanjangPlaintext; $i += 3)  {
      // Pangkas 6 Bit Pertama Jadi Nilai Decimal
      $VariablePenampungPlaintext = (ord($Plaintext[$i]) & 252) >> 2;

      // Mencari Nilai Index Ke-1 Key dan Melakukan ROT47 Pada Key Tersebut
      // $HasilKey[] = $VariablePenampungKey = (ord($Key[$i % $PanjangKey]) & 252) >> 2;

      // Melakukan XOR Antara Index Ke-1 Plaintext Dengan Index Ke-1 Key Yg Telah Di ROT47
      // $VariablePenampungPlaintext = (($VariablePenampungPlaintext+47)%64) ^ $VariablePenampungKey;
      $Debug = 'N' . ($loopKeyXOR+1) . ' = ' . sprintf("%06s", decbin($VariablePenampungPlaintext)) . '<br>';

      $Debug .= 'N' . ($loopKeyXOR+1) . ' = ' . $VariablePenampungPlaintext . '<br>';

      $Debug .= 'N' . ($loopKeyXOR+1) . ' = ' . '((' . $VariablePenampungPlaintext . '+' . $SecretAdding . ') mod 64) xor '. $IndexKey[($loopKeyXOR) % $PanjangKey]. '<br>';

      $Debug .= 'N' . ($loopKeyXOR+1) . ' = ' . sprintf("%06s", decbin(($VariablePenampungPlaintext+$SecretAdding)%64)) . ' xor '. sprintf("%06s", decbin($IndexKey[($loopKeyXOR) % $PanjangKey])). '<br>';

      $VariablePenampungPlaintext = (($VariablePenampungPlaintext+$SecretAdding)%64) ^ $IndexKey[($loopKeyXOR++) % $PanjangKey];

      $Debug .= ' N' . ($loopKeyXOR) . ' = ' . sprintf("%06s", decbin($VariablePenampungPlaintext)). '<br>';
      $Debug .= ' N' . ($loopKeyXOR) . ' = ' . $VariablePenampungPlaintext. '<br>';
      $Debug .= ' N' . ($loopKeyXOR) . ' = (' . $VariablePenampungPlaintext . " + 47) mod 64" . '<br>';
      $Debug .= ' N' . ($loopKeyXOR) . ' = ' . (($VariablePenampungPlaintext + 47)%64) . '<br>';
      $Debug .= ' N' . ($loopKeyXOR) . ' = ' . $NilaiIndexBase64[$VariablePenampungPlaintext]. '<br>';

      // Menampung Data Dalam Bentu Decimal, Hexa, Dan Binary Pada Index Ke 1
      $DataIndexDebug[$Hitung]['1']   = base64_encode($Debug);
      $DataIndexDecimal[$Hitung]['1'] = $VariablePenampungPlaintext;
      $DataIndexHexa[$Hitung]['1']    = sprintf("%02s", dechex($VariablePenampungPlaintext));
      $DataIndexBin[$Hitung]['1']     = sprintf("%06s", decbin($VariablePenampungPlaintext));

      // Hasil Pangkas 6 Bit Pertama
      $HasilEncode .= ($NilaiIndexBase64[$VariablePenampungPlaintext]);
      
      // Pangkas 6 Bit Kedua
      $VariablePenampungPlaintext = (ord($Plaintext[$i]) & 3) << 4;
      // $VariablePenampungKey       = (ord($Key[$i % $PanjangKey]) & 3) << 4;
      if ($i + 1 < $PanjangPlaintext) {

        $VariablePenampungPlaintext |= (ord($Plaintext[$i + 1]) & 240) >> 4;

        // Mencari Nilai Index Ke-2 Key dan Melakukan ROT47 Pada Key Tersebut
        // $HasilKey[] = $VariablePenampungKey = (ord($Key[($i+1) % $PanjangKey]) & 240) >> 4;

        // Melakukan XOR Antara Index Ke-2 Plaintext Dengan Index Ke-2 Key Yg Telah Di ROT47
        // $VariablePenampungPlaintext = (($VariablePenampungPlaintext+47)%64) ^ $VariablePenampungKey;
        $Debug = 'N' . ($loopKeyXOR+1) . ' = ' . sprintf("%06s", decbin($VariablePenampungPlaintext)) . '<br>';
        
        $Debug .= 'N' . ($loopKeyXOR+1) . ' = ' . $VariablePenampungPlaintext . '<br>';
        
        $Debug .= 'N' . ($loopKeyXOR+1) . ' = ' . '((' . $VariablePenampungPlaintext . '+' . $SecretAdding . ') mod 64) xor '. $IndexKey[($loopKeyXOR) % $PanjangKey]. '<br>';

        $Debug .= 'N' . ($loopKeyXOR+1) . ' = ' . sprintf("%06s", decbin(($VariablePenampungPlaintext+$SecretAdding)%64)) . ' xor '. sprintf("%06s", decbin($IndexKey[($loopKeyXOR) % $PanjangKey])). '<br>';

        $VariablePenampungPlaintext = (($VariablePenampungPlaintext+$SecretAdding)%64) ^ $IndexKey[($loopKeyXOR++) % $PanjangKey];

        $Debug .= ' N' . ($loopKeyXOR) . ' = ' . sprintf("%06s", decbin($VariablePenampungPlaintext)). '<br>';
        $Debug .= ' N' . ($loopKeyXOR) . ' = ' . $VariablePenampungPlaintext. '<br>';
        $Debug .= ' N' . ($loopKeyXOR) . ' = (' . $VariablePenampungPlaintext . " + 47) mod 64" . '<br>';
        $Debug .= ' N' . ($loopKeyXOR) . ' = ' . (($VariablePenampungPlaintext + 47)%64) . '<br>';
        $Debug .= ' N' . ($loopKeyXOR) . ' = ' . $NilaiIndexBase64[$VariablePenampungPlaintext]. '<br>';
        

        // Menampung Data Dalam Bentu Decimal, Hexa, Dan Binary Pada Index Ke 2
        $DataIndexDebug[$Hitung]['2']   = base64_encode($Debug);
        $DataIndexDecimal[$Hitung]['2'] = $VariablePenampungPlaintext;
        $DataIndexHexa[$Hitung]['2']    = sprintf("%02s", dechex($VariablePenampungPlaintext));
        $DataIndexBin[$Hitung]['2']     = sprintf("%06s", decbin($VariablePenampungPlaintext));

        // Hasil 6 Bit Kedua Jika Plaintext Kelipatan Lebih Dari 2
        $HasilEncode .= ($NilaiIndexBase64[$VariablePenampungPlaintext]);

        // Pangkas 6 Bit Ketiga
        $VariablePenampungPlaintext = (ord($Plaintext[$i + 1]) & 15) << 2;
        // $VariablePenampungKey       = (ord($Key[( $i + 1 ) % $PanjangKey]) & 15) << 2;

        if ($i + 2 < $PanjangPlaintext)  {

          $VariablePenampungPlaintext |= (ord($Plaintext[$i + 2]) & 192) >> 6;

          // Mencari Nilai Index Ke-3 Key dan Melakukan ROT47 Pada Key Tersebut
          // $HasilKey[] = $VariablePenampungKey = (ord($Key[($i+2) % $PanjangKey]) & 192) >> 6;

          // Melakukan XOR Antara Index Ke-3 Plaintext Dengan Index Ke-3 Key Yg Telah Di ROT47
          // $VariablePenampungPlaintext = (($VariablePenampungPlaintext+47)%64) ^ $VariablePenampungKey;
          $Debug = 'N' . ($loopKeyXOR+1) . ' = ' . sprintf("%06s", decbin($VariablePenampungPlaintext)) . '<br>';

          $Debug .= 'N' . ($loopKeyXOR+1) . ' = ' . $VariablePenampungPlaintext . '<br>';

          $Debug .= 'N' . ($loopKeyXOR+1) . ' = ' . '((' . $VariablePenampungPlaintext . '+' . $SecretAdding . ') mod 64) xor '. $IndexKey[($loopKeyXOR) % $PanjangKey]. '<br>';

          $Debug .= 'N' . ($loopKeyXOR+1) . ' = ' . sprintf("%06s", decbin(($VariablePenampungPlaintext+$SecretAdding)%64)) . ' xor '. sprintf("%06s", decbin($IndexKey[($loopKeyXOR) % $PanjangKey])). '<br>';

          $VariablePenampungPlaintext = (($VariablePenampungPlaintext+$SecretAdding)%64) ^ $IndexKey[($loopKeyXOR++) % $PanjangKey];

          $Debug .= ' N' . ($loopKeyXOR) . ' = ' . sprintf("%06s", decbin($VariablePenampungPlaintext)). '<br>';
          $Debug .= ' N' . ($loopKeyXOR) . ' = ' . $VariablePenampungPlaintext. '<br>';
          $Debug .= ' N' . ($loopKeyXOR) . ' = (' . $VariablePenampungPlaintext . " + 47) mod 64" . '<br>';
          $Debug .= ' N' . ($loopKeyXOR) . ' = ' . (($VariablePenampungPlaintext + 47)%64) . '<br>';
          $Debug .= ' N' . ($loopKeyXOR) . ' = ' . $NilaiIndexBase64[$VariablePenampungPlaintext]. '<br>';

          // Menampung Data Dalam Bentu Decimal, Hexa, Dan Binary Pada Index Ke 3
          $DataIndexDebug[$Hitung]['3']   = base64_encode($Debug);
          $DataIndexDecimal[$Hitung]['3'] = $VariablePenampungPlaintext;
          $DataIndexHexa[$Hitung]['3']    = sprintf("%02s", dechex($VariablePenampungPlaintext));
          $DataIndexBin[$Hitung]['3']     = sprintf("%06s", decbin($VariablePenampungPlaintext));

          // Hasil 6 Bit Ketiga Jika Plaintext Kelipatan Lebih Dari 3
          $HasilEncode .= ($NilaiIndexBase64[$VariablePenampungPlaintext]);
          
          // Pangkas Index Ke Empat
          $VariablePenampungPlaintext = ord($Plaintext[$i + 2]) & 63;

          // Mencari Nilai Index Ke-4 Key dan Melakukan ROT47 Pada Key Tersebut
          // $HasilKey[] = $VariablePenampungKey = (ord($Key[($i+2) % $PanjangKey]) & 63);

          // Melakukan XOR Antara Index Ke-4 Plaintext Dengan Index Ke-4 Key Yg Telah Di ROT47
          // $VariablePenampungPlaintext = (($VariablePenampungPlaintext+47)%64) ^ $VariablePenampungKey;
          // $VariablePenampungPlaintext = (($VariablePenampungPlaintext+47)%64) ^ $IndexKey[($i+2) % $PanjangKey];;
          $Debug = 'N' . ($loopKeyXOR+1) . ' = ' . sprintf("%06s", decbin($VariablePenampungPlaintext)) . '<br>';

          $Debug .= 'N' . ($loopKeyXOR+1) . ' = ' . $VariablePenampungPlaintext . '<br>';

          $Debug .= 'N' . ($loopKeyXOR+1) . ' = ' . '((' . $VariablePenampungPlaintext . '+' . $SecretAdding . ') mod 64) xor '. $IndexKey[($loopKeyXOR) % $PanjangKey]. '<br>';

          $Debug .= 'N' . ($loopKeyXOR+1) . ' = ' . sprintf("%06s", decbin(($VariablePenampungPlaintext+$SecretAdding)%64)) . ' xor '. sprintf("%06s", decbin($IndexKey[($loopKeyXOR) % $PanjangKey])). '<br>';

          $VariablePenampungPlaintext = (($VariablePenampungPlaintext+$SecretAdding)%64) ^ $IndexKey[($loopKeyXOR++) % $PanjangKey];

          $Debug .= ' N' . ($loopKeyXOR) . ' = ' . sprintf("%06s", decbin($VariablePenampungPlaintext)). '<br>';
          $Debug .= ' N' . ($loopKeyXOR) . ' = ' . $VariablePenampungPlaintext. '<br>';
          $Debug .= ' N' . ($loopKeyXOR) . ' = (' . $VariablePenampungPlaintext . " + 47) mod 64" . '<br>';
          $Debug .= ' N' . ($loopKeyXOR) . ' = ' . (($VariablePenampungPlaintext + 47)%64) . '<br>';
          $Debug .= ' N' . ($loopKeyXOR) . ' = ' . $NilaiIndexBase64[$VariablePenampungPlaintext]. '<br>';

          // Menampung Data Dalam Bentu Decimal, Hexa, Dan Binary Pada Index Ke 4
          $DataIndexDebug[$Hitung]['4']   = base64_encode($Debug);
          $DataIndexDecimal[$Hitung]['4'] = $VariablePenampungPlaintext;
          $DataIndexHexa[$Hitung]['4']    = sprintf("%02s", dechex($VariablePenampungPlaintext));
          $DataIndexBin[$Hitung]['4']     = sprintf("%06s", decbin($VariablePenampungPlaintext));
          
          // Hasil Index Ke Empat
          $HasilEncode .= ($NilaiIndexBase64[$VariablePenampungPlaintext]);

        } else  {

          // Mencari Nilai Index Ke-3 Key dan Melakukan ROT47 Pada Key Tersebut
          // $HasilKey[] = $VariablePenampungKey;

          // Melakukan XOR Antara Index Ke-3 Plaintext Dengan Index Ke-3 Key Yg Telah Di ROT47
          // $VariablePenampungPlaintext = (($VariablePenampungPlaintext+47)%64) ^ $VariablePenampungKey;
          $Debug = 'N' . ($loopKeyXOR+1) . ' = ' . sprintf("%06s", decbin($VariablePenampungPlaintext)) . '<br>';

          $Debug .= 'N' . ($loopKeyXOR+1) . ' = ' . $VariablePenampungPlaintext . '<br>';

          $Debug .= 'N' . ($loopKeyXOR+1) . ' = ' . '((' . $VariablePenampungPlaintext . '+' . $SecretAdding . ') mod 64) xor '. $IndexKey[($loopKeyXOR) % $PanjangKey]. '<br>';

          $Debug .= 'N' . ($loopKeyXOR+1) . ' = ' . sprintf("%06s", decbin(($VariablePenampungPlaintext+$SecretAdding)%64)) . ' xor '. sprintf("%06s", decbin($IndexKey[($loopKeyXOR) % $PanjangKey])). '<br>';

          $VariablePenampungPlaintext = (($VariablePenampungPlaintext+$SecretAdding)%64) ^ $IndexKey[($loopKeyXOR++) % $PanjangKey];

          $Debug .= ' N' . ($loopKeyXOR) . ' = ' . sprintf("%06s", decbin($VariablePenampungPlaintext)). '<br>';
          $Debug .= ' N' . ($loopKeyXOR) . ' = ' . $VariablePenampungPlaintext. '<br>';
          $Debug .= ' N' . ($loopKeyXOR) . ' = (' . $VariablePenampungPlaintext . " + 47) mod 64" . '<br>';
          $Debug .= ' N' . ($loopKeyXOR) . ' = ' . (($VariablePenampungPlaintext + 47)%64) . '<br>';
          $Debug .= ' N' . ($loopKeyXOR) . ' = ' . $NilaiIndexBase64[$VariablePenampungPlaintext]. '<br>';

          // Menampung Data Dalam Bentu Decimal, Hexa, Dan Binary Pada Index Ke 3
          $DataIndexDebug[$Hitung]['3']   = base64_encode($Debug);
          $DataIndexDecimal[$Hitung]['3'] = $VariablePenampungPlaintext;
          $DataIndexHexa[$Hitung]['3']    = sprintf("%02s", dechex($VariablePenampungPlaintext));
          $DataIndexBin[$Hitung]['3']     = sprintf("%06s", decbin($VariablePenampungPlaintext));

          // Hasil Index Ke Tiga Jika Plaintext Kelipatan Kurang Dari 3
          $HasilEncode .= ($NilaiIndexBase64[$VariablePenampungPlaintext]);
          $HasilEncode .= ('=');
        }
      } else{
        // Mencari Nilai Index Ke-2 Key dan Melakukan ROT47 Pada Key Tersebut
        // $HasilKey[] = $VariablePenampungKey;

        // Melakukan XOR Antara Index Ke-2 Plaintext Dengan Index Ke-2 Key Yg Telah Di ROT47
        // $VariablePenampungPlaintext = (($VariablePenampungPlaintext+47)%64) ^ $VariablePenampungKey;
        $Debug = 'N' . ($loopKeyXOR+1) . ' = ' . sprintf("%06s", decbin($VariablePenampungPlaintext)) . '<br>';
        
        $Debug .= 'N' . ($loopKeyXOR+1) . ' = ' . $VariablePenampungPlaintext . '<br>';
        
        $Debug .= 'N' . ($loopKeyXOR+1) . ' = ' . '((' . $VariablePenampungPlaintext . '+' . $SecretAdding . ') mod 64) xor '. $IndexKey[($loopKeyXOR) % $PanjangKey]. '<br>';

        $Debug .= 'N' . ($loopKeyXOR+1) . ' = ' . sprintf("%06s", decbin(($VariablePenampungPlaintext+$SecretAdding)%64)) . ' xor '. sprintf("%06s", decbin($IndexKey[($loopKeyXOR) % $PanjangKey])). '<br>';

        $VariablePenampungPlaintext = (($VariablePenampungPlaintext+$SecretAdding)%64) ^ $IndexKey[($loopKeyXOR++) % $PanjangKey];

        $Debug .= ' N' . ($loopKeyXOR) . ' = ' . sprintf("%06s", decbin($VariablePenampungPlaintext)). '<br>';
        $Debug .= ' N' . ($loopKeyXOR) . ' = ' . $VariablePenampungPlaintext. '<br>';
        $Debug .= ' N' . ($loopKeyXOR) . ' = (' . $VariablePenampungPlaintext . " + 47) mod 64" . '<br>';
        $Debug .= ' N' . ($loopKeyXOR) . ' = ' . (($VariablePenampungPlaintext + 47)%64) . '<br>';
        $Debug .= ' N' . ($loopKeyXOR) . ' = ' . $NilaiIndexBase64[$VariablePenampungPlaintext]. '<br>';

        // Menampung Data Dalam Bentu Decimal, Hexa, Dan Binary Pada Index Ke 2
        $DataIndexDebug[$Hitung]['2']   = base64_encode($Debug);
        $DataIndexDecimal[$Hitung]['2'] = $VariablePenampungPlaintext;
        $DataIndexHexa[$Hitung]['2']    = sprintf("%02s", dechex($VariablePenampungPlaintext));
        $DataIndexBin[$Hitung]['2']     = sprintf("%06s", decbin($VariablePenampungPlaintext));

        // Hasil 6 Bit Kedua Jika Plaintext Hanya 1 Karakter
        $HasilEncode .= ($NilaiIndexBase64[$VariablePenampungPlaintext]);
        $HasilEncode .= ("==");
      }
      $Hitung++;
    }
    // return $HasilEncode;
    return [
      "HasilEncode" => base64_encode($HasilEncode),
      "HasilKey" => $IndexKey,
      "Index" => [
        "Debug" => $DataIndexDebug,
        "Decimal" => $DataIndexDecimal,
        "Hexa" => $DataIndexHexa,
        "Binary" => $DataIndexBin,
      ]
    ];
  }

  public function decode_debug($Chipertext, $Key){
    $NilaiIndexBase64 = $this->NilaiIndexBase64;

    try {
      // Memeriksa Chipertext Tidak Kosong
      if($Chipertext == null || $Key == null)
      {
        // Pesan Bahwa Chipertext Kosong
        throw new \Exception("INPUT IS NULL");
      }
    } catch (\Exception $e) {
      return (isset($e->xdebug_message)? $e->xdebug_message : $e->getMessage());
    }

    try {
      // Memeriksa Format Chipertext
      if (strlen($Chipertext) % 4 != 0)
      {
        // Pesan Bahwa Format Chipertext Tidak Sesuai
        throw new \Exception("INVALID BASE64 STRING");
      }
    } catch (\Exception $e) {
      return (isset($e->xdebug_message)? $e->xdebug_message : $e->getMessage());
    }

    $HitungPanjangPlaintext = strlen(base64_decode($Chipertext));

    $PlaintextChars = str_split($Chipertext);

    $PanjangKey = (strlen($Key) < $HitungPanjangPlaintext) ? strlen($Key) : $HitungPanjangPlaintext;
    $Key = str_split($Key);

    $IndexKey = array();
    for ($LoopKey = 0; $LoopKey < $PanjangKey; $LoopKey += 3)  {
      // Pangkas 6 Bit Pertama Jadi Nilai Decimal
      $VariablePenampungKey = (ord($Key[$LoopKey]) & 252) >> 2;

      // Index Key Ke $LoopKey
      $IndexKey[] = $VariablePenampungKey;

      // Pangkas 6 Bit Kedua
      $VariablePenampungKey = (ord($Key[$LoopKey]) & 3) << 4;

      if ($LoopKey + 1 < $PanjangKey) {

        $VariablePenampungKey |= (ord($Key[$LoopKey + 1]) & 240) >> 4;

        // Index Key Ke $LoopKey+1
        $IndexKey[] = $VariablePenampungKey;

        // Pangkas 6 Bit Ketiga
        $VariablePenampungKey = (ord($Key[$LoopKey + 1]) & 15) << 2;

        if ($LoopKey + 2 < $PanjangKey)  {

          $VariablePenampungKey |= (ord($Key[$LoopKey + 2]) & 192) >> 6;

          // Index Key Ke $LoopKey+2
          $IndexKey[] = $VariablePenampungKey;

          // Pangkas Index Ke Empat
          $VariablePenampungKey = ord($Key[$LoopKey + 2]) & 63;

          // Index Key Ke $LoopKey+3
          $IndexKey[] = $VariablePenampungKey;

        } else  {

          // Index Key Ke $LoopKey+2
          $IndexKey[] = $VariablePenampungKey;
        }
      } else {

        // Index Key Ke $LoopKey+1
        $IndexKey[] = $VariablePenampungKey;
      }
    }

    $PanjangKey = count($IndexKey);

    // ((jumlah / panjang key)+47)%64
    // $SecretAdding = ((int)(array_sum($IndexKey) / $PanjangKey)+47)%64;
    // ((jumlah)+47)%64
    // $SecretAdding = ((int)(array_sum($IndexKey))+47)%64;
    // Ditambah +47
    $SecretAdding = 47;

    $j = 0;

    $Hitung = 1;

    $VariablePenampungPlaintext = array();

    for ($i = 0; $i < count($PlaintextChars); $i += 4) {
      $VariablePenampungPlaintext[0] = (((strpos($NilaiIndexBase64,$PlaintextChars[$i]) ^ $IndexKey[($i % $PanjangKey)])+64)-$SecretAdding)%64;
      $VariablePenampungPlaintext[1] = (((strpos($NilaiIndexBase64,$PlaintextChars[$i+1]) ^ $IndexKey[($i+1) % $PanjangKey])+64)-$SecretAdding)%64;
      $VariablePenampungPlaintext[2] = (((strpos($NilaiIndexBase64,$PlaintextChars[$i+2]) ^ $IndexKey[($i+2) % $PanjangKey])+64)-$SecretAdding)%64;
      $VariablePenampungPlaintext[3] = (((strpos($NilaiIndexBase64,$PlaintextChars[$i+3]) ^ $IndexKey[($i+3) % $PanjangKey])+64)-$SecretAdding)%64;

      $PlaintextChars[$i]!="=" ? $VariablePenampungPlaintext[0] = $VariablePenampungPlaintext[0] : $VariablePenampungPlaintext[0] = 999;
      $PlaintextChars[$i+1]!="=" ? $VariablePenampungPlaintext[1] = $VariablePenampungPlaintext[1] : $VariablePenampungPlaintext[1] = 999;
      $PlaintextChars[$i+2]!="=" ? $VariablePenampungPlaintext[2] = $VariablePenampungPlaintext[2] : $VariablePenampungPlaintext[2] = 999;
      $PlaintextChars[$i+3]!="=" ? $VariablePenampungPlaintext[3] = $VariablePenampungPlaintext[3] : $VariablePenampungPlaintext[3] = 999;
      
      $Debug = 'N' . ($j+1) . ' = ( ( ( ( ( ' . strpos($NilaiIndexBase64,$PlaintextChars[$i]) . " xor " . $IndexKey[($i % $PanjangKey)] . ' ) + 64 ) - ' . $SecretAdding . ') mod 64) SHIFT LEFT 2) OR ( ( ( (' . strpos($NilaiIndexBase64,$PlaintextChars[($i+1)]) . " xor " . $IndexKey[(($i+1) % $PanjangKey)] . ' ) + 64 ) - ' . $SecretAdding . ') mod 64) SHIFT RIGHT 4 )' . '<br>';

      $Debug .= 'N' . ($j+1) . ' = ( ( ' . $VariablePenampungPlaintext[0] . ' SHIFT LEFT 2 ) OR ( ' . $VariablePenampungPlaintext[1] . ' SHIFT RIGHT 4 ) )' . '<br>';

      $Debug .= 'N' . ($j+1) . ' = ( ( ' . sprintf("%08s", decbin($VariablePenampungPlaintext[0])) . ' SHIFT LEFT 2 ) OR ( ' . sprintf("%08s", decbin($VariablePenampungPlaintext[1])) . ' SHIFT RIGHT 4 ) )' . '<br>';

      $Debug .= 'N' . ($j+1) . ' = ' . sprintf("%08s", decbin($VariablePenampungPlaintext[0] << 2)) . " OR " . sprintf("%08s", decbin($VariablePenampungPlaintext[1] >> 4)) . '<br>';

      $DataDecode = $VariablePenampungDecode[$j++] = (($VariablePenampungPlaintext[0] << 2) | ($VariablePenampungPlaintext[1] >> 4));

      $Debug .= 'N' . ($j) . ' = ' . sprintf("%08s", decbin($DataDecode % 256)) . '<br>';
      $Debug .= 'N' . ($j) . ' = ' . ($DataDecode % 256) . '<br>';
      $Debug .= 'N' . ($j) . ' = ' . chr(($DataDecode % 256)) . '<br>';

      $DataIndexDebug[$Hitung]['1']   = base64_encode($Debug);
      $DataIndexDecimal[$Hitung]['1'] = $DataDecode % 256;
      $DataIndexHexa[$Hitung]['1']    = sprintf("%02s", dechex($VariablePenampungDecode[($j-1)] % 256));
      $DataIndexBin[$Hitung]['1']     = sprintf("%08s", decbin($VariablePenampungDecode[($j-1)] % 256));

      if ($VariablePenampungPlaintext[2] < 64)
      {
        $Debug = 'N' . ($j+1) . ' = ( ( ( ( ( ' . strpos($NilaiIndexBase64,$PlaintextChars[$i+1]) . " xor " . $IndexKey[(($i+1) % $PanjangKey)] . ' ) + 64 ) - ' . $SecretAdding . ') mod 64) SHIFT LEFT 4) OR ( ( ( (' . strpos($NilaiIndexBase64,$PlaintextChars[($i+2)]) . " xor " . $IndexKey[(($i+2) % $PanjangKey)] . ' ) + 64 ) - ' . $SecretAdding . ') mod 64) SHIFT RIGHT 2 )' . '<br>';

        $Debug .= 'N' . ($j+1) . ' = ( ( ' . $VariablePenampungPlaintext[1] . ' SHIFT LEFT 4 ) OR ( ' . $VariablePenampungPlaintext[2] . ' SHIFT RIGHT 2 ) )' . '<br>';

        $Debug .= 'N' . ($j+1) . ' = ( ( ' . sprintf("%08s", decbin($VariablePenampungPlaintext[1])) . 'SHIFT LEFT 4 ) OR ( ' . sprintf("%08s", decbin($VariablePenampungPlaintext[2])) . ' SHIFT RIGHT 2 ) )' . '<br>';

        $Debug .= 'N' . ($j+1) . ' = ' . sprintf("%08s", decbin($VariablePenampungPlaintext[1] << 4)) . " OR " . sprintf("%08s", decbin($VariablePenampungPlaintext[2] >> 2)) . '<br>';

        $DataDecode = $VariablePenampungDecode[$j++] = (($VariablePenampungPlaintext[1] << 4) | ($VariablePenampungPlaintext[2] >> 2));

        $Debug .= 'N' . ($j) . ' = ' . sprintf("%08s", decbin($DataDecode % 256)) . '<br>';
        $Debug .= 'N' . ($j) . ' = ' . ($DataDecode % 256) . '<br>';
        $Debug .= 'N' . ($j) . ' = ' . chr(($DataDecode % 256)) . '<br>';

        $DataIndexDebug[$Hitung]['2']   = base64_encode($Debug);
        $DataIndexDecimal[$Hitung]['2'] = $DataDecode % 256;
        $DataIndexHexa[$Hitung]['2']    = sprintf("%02s", dechex($VariablePenampungDecode[($j-1)] % 256));
        $DataIndexBin[$Hitung]['2']     = sprintf("%08s", decbin($VariablePenampungDecode[($j-1)] % 256));

        if ($VariablePenampungPlaintext[3] < 64)  {
          $Debug = 'N' . ($j+1) . ' = ( ( ( ( ( ' . strpos($NilaiIndexBase64,$PlaintextChars[$i+2]) . " xor " . $IndexKey[(($i+2) % $PanjangKey)] . ' ) + 64 ) - ' . $SecretAdding . ') mod 64) SHIFT LEFT 6) OR ( ( ( (' . strpos($NilaiIndexBase64,$PlaintextChars[($i+3)]) . " xor " . $IndexKey[(($i+3) % $PanjangKey)] . ' ) + 64 ) - ' . $SecretAdding . ') mod 64))' . '<br>';

          $Debug .= 'N' . ($j+1) . ' = ( ( ' . $VariablePenampungPlaintext[2] . ' SHIFT LEFT 6 ) OR ( ' . $VariablePenampungPlaintext[3] . ') )' . '<br>';

          $Debug .= 'N' . ($j+1) . ' = ( ( ' . sprintf("%08s", decbin($VariablePenampungPlaintext[2])) . 'SHIFT LEFT 6 ) OR ( ' . sprintf("%08s", decbin($VariablePenampungPlaintext[3])) . ') )' . '<br>';

          $Debug .= 'N' . ($j+1) . ' = ' . sprintf("%08s", decbin($VariablePenampungPlaintext[2] << 6)) . " OR " . sprintf("%08s", decbin($VariablePenampungPlaintext[3])) . '<br>';

          $DataDecode = $VariablePenampungDecode[$j++] = (($VariablePenampungPlaintext[2] << 6) | $VariablePenampungPlaintext[3]);

          $Debug .= 'N' . ($j) . ' = ' . sprintf("%08s", decbin($DataDecode % 256)) . '<br>';
          $Debug .= 'N' . ($j) . ' = ' . ($DataDecode % 256) . '<br>';
          $Debug .= 'N' . ($j) . ' = ' . chr(($DataDecode % 256)) . '<br>';

          $DataIndexDebug[$Hitung]['3']   = base64_encode($Debug);
          $DataIndexDecimal[$Hitung]['3'] = $DataDecode % 256;
          $DataIndexHexa[$Hitung]['3']    = sprintf("%02s", dechex($VariablePenampungDecode[($j-1)] % 256));
          $DataIndexBin[$Hitung]['3']     = sprintf("%08s", decbin($VariablePenampungDecode[($j-1)] % 256));

        }
      }
      $Hitung++;
    }
    $HasilDecode = '';
    for($i=0;$i<$HitungPanjangPlaintext;$i++)
    {
      $HasilDecode .= chr($VariablePenampungDecode[$i]);

    }
    // return $HasilDecode;
    return [
      "HasilDecode" => base64_encode($HasilDecode),
      "Index" => [
        "Debug" => $DataIndexDebug,
        "Decimal" => $DataIndexDecimal,
        "Hexa" => $DataIndexHexa,
        "Binary" => $DataIndexBin,
      ],
      "IndexKey" => $IndexKey,
      "Penampung" => $VariablePenampungDecode,
      "PanjangKey" => $HitungPanjangPlaintext,
    ];
  }
}