<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Helper\ImageHandler;
use App\Http\Requests\ProductRequest;
use App\Transformers\ProductTransformer;

use App\Http\Controllers\Enc;

class ProductController extends Controller
{

    public function __construct(Product $product, ProductTransformer $productTransformer){
        $this->product = $product;
        $this->productTransformer = $productTransformer;
    }

    public function index(Request $request)
    {
        $limit = $request->input('limit') ?: 5;
        $products = $this->product->paginate($limit);
        return  $this->respondWithPagination($products, [
            'data' =>  $this->productTransformer->transformCollection($products)
        ]);
    }

    public function array_data(){
        $products = $this->product->orderBy('id')->select("id","name","price")->get(); 
        return response()->json($products);
    }

    public function searchByName($name){
        $token = base64_decode(request()->header('x-csrf-token'));
        $enc = new Enc();

        $products = $this->product->orderBy('name')->select("id","name","price")->where('name', 'like', '%' . $name . '%')->get(); 
        return response()->json($products);
    }

    
    public function store(ProductRequest $request)
    {
        $token = base64_decode(request()->header('x-csrf-token'));
        $enc = new Enc();

        $user = $this->getUser();
        if ($request->images!="") {

        	$image_name = ImageHandler::upload(
                $enc->decode($request->images,$token),
                $enc->decode($request->filename,$token),
                'products'
            );

        	if(is_null($image_name)){
        		return response()->json(['error' => true, 'message' => 'There is problem with image']);
        	}
        	$this->product->image_path = $enc->encode($image_name,"Mr20k");
        }
        
        $this->product->name = $enc->encode($enc->decode($request->name,$token),"Mr20k");
        $this->product->price = $enc->decode($request->price,$token);
        $this->product->description = $enc->encode($enc->decode($request->description,$token),"Mr20k");

        $this->product->last_updated_by = $user->id;;
        try{            
            $this->product->save();
        }catch(\Exception $e){
        	dd($e);
            return response()->json(['error' => true, 'message' => 'There is problem on server']);
        }

        return response()->json(['error' => false, 'message' => 'product success created']);

    }

    
    public function show($id)
    {
        $token = base64_decode(request()->header('x-csrf-token'));
        $enc = new Enc();
        
        $id = (int)$enc->decode($id,$token);

        $product = Product::find($id);  

        if(empty($product)){
            return $this->respondNotFound();
        }
        return response()->json($this->productTransformer->transform($product));
    }
    
    
    public function update(productRequest $request, $id)
    {
        $token = base64_decode(request()->header('x-csrf-token'));
        $enc = new Enc();
        
        $id = $enc->decode($id,$token);

        $user = $this->getUser();
        $product = $this->product->find($id);  
        
        if(empty($product)){
            return $this->respondNotFound();
        }

        if ($request->images!="") {
        	$image_name = ImageHandler::upload(
                $enc->decode($request->images,$token),
                $enc->decode($request->filename,$token),
                'products'
            );
        	if(is_null($image_name)){
        		return response()->json(['error' => true, 'message' => 'There is problem with image']);
        	}
        	$product->image_path = $enc->encode($image_name,"Mr20k");
        }
        
        $product->name = $enc->encode($enc->decode($request->name,$token),"Mr20k");
        $product->price = $enc->decode($request->price,$token);
        $product->description = $enc->encode($enc->decode($request->description,$token),"Mr20k");

        $product->last_updated_by = $user->id;

        try{            
            $product->save();
        }catch(\Exception $e){
            dd($e);
            return response()->json(['error' => true, 'message' => 'There is problem on server']);
        }

        return response()->json(['error' => false, 'message' => 'product successfully updated']);
    }

    
    public function destroy($id)
    {
        $token = base64_decode(request()->header('x-csrf-token'));
        $enc = new Enc();
        
        $id = $enc->decode($id,$token);

        $product = $this->product->find($id);
        if(empty($product)){
            return $this->respondNotFound();
        }
        try{            
            $product->delete();
        }catch(\Exception $e){
            //dd($e);
            if($e->getCode() == "23000"){
                return response()->json(['error' => true, 'message' => 'Failed, Other Data Refrence this data']);
            }
            return response()->json(['error' => true, 'message' => 'There is problem on server']);
        }

        return response()->json(['error' => false, 'message' => 'product success deleted']);

    }
}
