<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Enc;

class TestAlgoritm extends Controller
{

	public function encrypt(){
		$enc = new Enc();
		return response()->json([
			'status' => 'Enkripsi',
			'Plaintext' => request('text'),
			'Key' => request('key'),
			'result' => base64_encode($enc->encode(request('text'), request('key'))),
			'result_debug' => $enc->encode_debug(request('text'), request('key'))
	]);
	}

	public function decrypt(){
		$enc = new Enc();
		return response()->json([
			'status' => 'Deskripsi',
			'Chipertext' => request('text'),
			'Key' => request('key'),
			'result' => base64_encode($enc->decode(request('text'), request('key'))),
			'result_debug' => $enc->decode_debug(request('text'), request('key'))
		]);
	}



}
