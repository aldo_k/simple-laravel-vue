<?php

namespace App\Transformers;

use App\Http\Controllers\Enc;

class ProductTransformer extends Transformer {

    public function transform($data)
    {
        $token = base64_decode(request()->header('x-csrf-token'));
        $enc = new Enc();

        if(!empty($data["image_path"])){
            $image_path = $enc->decode($data["image_path"],"Mr20k");
            $smallPath = url('images/products/small/'.$image_path);
            $largePath = url('images/products/large/'.$image_path);
        }else{
            $smallPath = "";
            $largePath = "";
        }
        
        return [
            'id' => $enc->encode($data['id'], $token),
            'name' => $enc->encode($enc->decode($data['name'],"Mr20k"), $token),  
            'price' => $enc->encode($data['price'], $token),  
            'description' => $enc->encode($enc->decode($data["description"],"Mr20k"), $token),
            'small_image_path' => $enc->encode($smallPath, $token),
            'large_image_path' => $enc->encode($largePath, $token),
            'last_updated_by' => $enc->encode($data->user->name, $token),
            'created_at' => $enc->encode($data->getReadableCreatedAt(), $token),
            'updated_at' => $enc->encode($data->getReadableUpdatedAt(), $token),
        ];
    }
}
