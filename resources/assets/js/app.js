
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import VueRouter from 'vue-router';
import router from './routes';
import userStore from  './UserStore';
import Vue from 'vue'

window.Vue = Vue;
Vue.use(VueRouter);


window._ = require('lodash');
window.enc = require('./enc');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 Vue.component('home', require('./components/Home.vue').default);
 Vue.component('navbar', require('./components/Navbar.vue').default);
 Vue.component('layout', require('./components/Layout.vue').default);
 Vue.component('login', require('./components/Login.vue').default);
 Vue.component('sidebar', require('./components/Sidebar.vue').default)
 Vue.component('pagination', require('./components/Pagination.vue').default);
 Vue.component('autocomplete', require('./components/AutoComplete.vue').default);

 axios.defaults.baseURL = 'http://127.0.0.1:'+window.location.port+'/';

 const app = new Vue({
 	el: '#app',
 	// mode: 'history',
 	router,
 	store: userStore,
 });
