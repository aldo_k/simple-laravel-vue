class Enc {
  constructor() {
    this.NilaiIndexBaseXorRot47 = "vwxyz0123456789+/ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstu=";
    this.encode = function (Plaintext, Key) {
      var NilaiIndexBaseXorRot47 = this.NilaiIndexBaseXorRot47;
      var PanjangPlaintext = this.strlen(Plaintext);
      var Plaintext = this.str_split(Plaintext);

      var PanjangKey = (this.strlen(Key) < PanjangPlaintext) ? this.strlen(Key) : PanjangPlaintext;

      var Key = this.str_split(Key);
      var IndexKey = new Array();
      var i = 0;
      for (var LoopKey = 0; LoopKey < PanjangKey; LoopKey += 3) {
        // Pangkas 6 Bit Pertama Jadi Nilai Decimal
        VariablePenampungKey = (this.ord(Key[LoopKey]) & 252) >> 2;

        // Index Key Ke LoopKey
        IndexKey[i++] = VariablePenampungKey;

        // Pangkas 6 Bit Kedua
        VariablePenampungKey = (this.ord(Key[LoopKey]) & 3) << 4;

        if (LoopKey + 1 < PanjangKey) {

          VariablePenampungKey |= (this.ord(Key[LoopKey + 1]) & 240) >> 4;

          // Index Key Ke LoopKey+1
          IndexKey[i++] = VariablePenampungKey;

          // Pangkas 6 Bit Ketiga
          VariablePenampungKey = (this.ord(Key[LoopKey + 1]) & 15) << 2;

          if (LoopKey + 2 < PanjangKey) {

            VariablePenampungKey |= (this.ord(Key[LoopKey + 2]) & 192) >> 6;

            // Index Key Ke LoopKey+2
            IndexKey[i++] = VariablePenampungKey;

            // Pangkas Index Ke Empat
            VariablePenampungKey = this.ord(Key[LoopKey + 2]) & 63;

            // Index Key Ke LoopKey+3
            IndexKey[i++] = VariablePenampungKey;

          } else {

            // Index Key Ke LoopKey+2
            IndexKey[i++] = VariablePenampungKey;
          }
        } else {

          // Index Key Ke LoopKey+1
          IndexKey[i++] = VariablePenampungKey;
        }
      }

      var PanjangKey = this.strlen(IndexKey);

      // var SecretAdding = (parseInt(this.array_sum(IndexKey) / PanjangKey)+47)%64;
      // var SecretAdding = (parseInt(this.array_sum(IndexKey))+47)%64;
      var SecretAdding = 47;

      var HasilEncode = '';
      var VariablePenampungPlaintext = '';
      var VariablePenampungKey = '';
      var Hitung = 1;

      var loopKeyXOR = 0;

      // Perulangan Menghitung Plaintext Per Tiga Karakter Untuk Dipisah Per 6 Bit
      for (var i = 0; i < PanjangPlaintext; i += 3) {
        // Pangkas 6 Bit Pertama Jadi Nilai Decimal
        VariablePenampungPlaintext = (this.ord(Plaintext[i]) & 252) >> 2;

        // Melakukan XOR Antara Index Ke-1 Plaintext Dengan Index Ke-1 Key Yg Telah Di ROT47
        VariablePenampungPlaintext = ((VariablePenampungPlaintext + SecretAdding) % 64) ^ IndexKey[(loopKeyXOR++) % PanjangKey];

        // Hasil Pangkas 6 Bit Pertama
        HasilEncode += (NilaiIndexBaseXorRot47[VariablePenampungPlaintext]);

        // Pangkas 6 Bit Kedua
        VariablePenampungPlaintext = (this.ord(Plaintext[i]) & 3) << 4;
        // VariablePenampungKey       = (ord(Key[i % PanjangKey]) & 3) << 4;
        if (i + 1 < PanjangPlaintext) {

          VariablePenampungPlaintext |= (this.ord(Plaintext[i + 1]) & 240) >> 4;

          // Melakukan XOR Antara Index Ke-2 Plaintext Dengan Index Ke-2 Key Yg Telah Di ROT47
          // VariablePenampungPlaintext = ((VariablePenampungPlaintext+47)%64) ^ VariablePenampungKey;
          VariablePenampungPlaintext = ((VariablePenampungPlaintext + SecretAdding) % 64) ^ IndexKey[(loopKeyXOR++) % PanjangKey];

          // Hasil 6 Bit Kedua Jika Plaintext Kelipatan Lebih Dari 2
          HasilEncode += (NilaiIndexBaseXorRot47[VariablePenampungPlaintext]);

          // Pangkas 6 Bit Ketiga
          VariablePenampungPlaintext = (this.ord(Plaintext[i + 1]) & 15) << 2;
          // VariablePenampungKey       = (ord(Key[( i + 1 ) % PanjangKey]) & 15) << 2;

          if (i + 2 < PanjangPlaintext) {

            VariablePenampungPlaintext |= (this.ord(Plaintext[i + 2]) & 192) >> 6;

            // Melakukan XOR Antara Index Ke-3 Plaintext Dengan Index Ke-3 Key Yg Telah Di ROT47
            VariablePenampungPlaintext = ((VariablePenampungPlaintext + SecretAdding) % 64) ^ IndexKey[(loopKeyXOR++) % PanjangKey];

            // Hasil 6 Bit Ketiga Jika Plaintext Kelipatan Lebih Dari 3
            HasilEncode += (NilaiIndexBaseXorRot47[VariablePenampungPlaintext]);

            // Pangkas Index Ke Empat
            VariablePenampungPlaintext = this.ord(Plaintext[i + 2]) & 63;

            // Melakukan XOR Antara Index Ke-4 Plaintext Dengan Index Ke-4 Key Yg Telah Di ROT47
            VariablePenampungPlaintext = ((VariablePenampungPlaintext + SecretAdding) % 64) ^ IndexKey[(loopKeyXOR++) % PanjangKey];;

            // Hasil Index Ke Empat
            HasilEncode += (NilaiIndexBaseXorRot47[VariablePenampungPlaintext]);

          } else {

            // Melakukan XOR Antara Index Ke-3 Plaintext Dengan Index Ke-3 Key Yg Telah Di ROT47
            VariablePenampungPlaintext = ((VariablePenampungPlaintext + SecretAdding) % 64) ^ IndexKey[(loopKeyXOR++) % PanjangKey];


            // Hasil Index Ke Tiga Jika Plaintext Kelipatan Kurang Dari 3
            HasilEncode += (NilaiIndexBaseXorRot47[VariablePenampungPlaintext]);
            HasilEncode += ('=');
          }
        } else {
          // Melakukan XOR Antara Index Ke-2 Plaintext Dengan Index Ke-2 Key Yg Telah Di ROT47
          VariablePenampungPlaintext = ((VariablePenampungPlaintext + SecretAdding) % 64) ^ IndexKey[(loopKeyXOR++) % PanjangKey];;

          // Hasil 6 Bit Kedua Jika Plaintext Hanya 1 Karakter
          HasilEncode += (NilaiIndexBaseXorRot47[VariablePenampungPlaintext]);
          HasilEncode += ("==");
        }
        Hitung++;
      }

      return HasilEncode;
    }
    this.decode = function (Chipertext, Key) {
      var NilaiIndexBaseXorRot47 = this.NilaiIndexBaseXorRot47;

      try {
        // Memeriksa Chipertext Tidak Kosong
        if (Chipertext == null || Key == null) {
          // Pesan Bahwa Chipertext Kosong
          throw new Error("INPUT IS NULL");
        }
      } catch (e) {
        console.error('Error!', e.message);
      }

      try {
        // Memeriksa Format Chipertext
        if (this.strlen(Chipertext) % 4 != 0) {
          // Pesan Bahwa Format Chipertext Tidak Sesuai
          throw new Error("INVALID BaseXorRot47 STRING");
        }
      } catch (e) {
        console.error('Error!', e.message);
      }

      var HitungPanjangPlaintext = this.strlen(atob(Chipertext));

      var PlaintextChars = this.str_split(Chipertext);

      var PanjangKey = (this.strlen(Key) < HitungPanjangPlaintext) ? this.strlen(Key) : HitungPanjangPlaintext;
      var Key = this.str_split(Key);

      var IndexKey = new Array();

      var i = 0;

      var VariablePenampungKey = 0;

      for (var LoopKey = 0; LoopKey < PanjangKey; LoopKey += 3) {
        // Pangkas 6 Bit Pertama Jadi Nilai Decimal
        VariablePenampungKey = (this.ord(Key[LoopKey]) & 252) >> 2;

        // Index Key Ke LoopKey
        IndexKey[i++] = VariablePenampungKey;

        // Pangkas 6 Bit Kedua
        VariablePenampungKey = (this.ord(Key[LoopKey]) & 3) << 4;

        if (LoopKey + 1 < PanjangKey) {

          VariablePenampungKey |= (this.ord(Key[LoopKey + 1]) & 240) >> 4;

          // Index Key Ke LoopKey+1
          IndexKey[i++] = VariablePenampungKey;

          // Pangkas 6 Bit Ketiga
          VariablePenampungKey = (this.ord(Key[LoopKey + 1]) & 15) << 2;

          if (LoopKey + 2 < PanjangKey) {

            VariablePenampungKey |= (this.ord(Key[LoopKey + 2]) & 192) >> 6;

            // Index Key Ke LoopKey+2
            IndexKey[i++] = VariablePenampungKey;

            // Pangkas Index Ke Empat
            VariablePenampungKey = this.ord(Key[LoopKey + 2]) & 63;

            // Index Key Ke LoopKey+3
            IndexKey[i++] = VariablePenampungKey;

          } else {

            // Index Key Ke LoopKey+2
            IndexKey[i++] = VariablePenampungKey;
          }
        } else {

          // Index Key Ke LoopKey+1
          IndexKey[i++] = VariablePenampungKey;
        }
      }

      var PanjangKey = this.strlen(IndexKey);

      // var SecretAdding = (parseInt(this.array_sum(IndexKey) / PanjangKey)+47)%64;
      // var SecretAdding = (parseInt(this.array_sum(IndexKey))+47)%64;
      var SecretAdding = 47;

      var j = 0;

      var Hitung = 1;

      var VariablePenampungPlaintext = new Array();
      var VariablePenampungDecode = new Array();

      var DataDecode = 0;
      for (var i = 0; i < this.strlen(PlaintextChars); i += 4) {
        VariablePenampungPlaintext[0] = (((this.strrpos(NilaiIndexBaseXorRot47, PlaintextChars[i]) ^ IndexKey[(i % PanjangKey)]) + 64) - SecretAdding) % 64;
        VariablePenampungPlaintext[1] = (((this.strrpos(NilaiIndexBaseXorRot47, PlaintextChars[i + 1]) ^ IndexKey[(i + 1) % PanjangKey]) + 64) - SecretAdding) % 64;
        VariablePenampungPlaintext[2] = (((this.strrpos(NilaiIndexBaseXorRot47, PlaintextChars[i + 2]) ^ IndexKey[(i + 2) % PanjangKey]) + 64) - SecretAdding) % 64;
        VariablePenampungPlaintext[3] = (((this.strrpos(NilaiIndexBaseXorRot47, PlaintextChars[i + 3]) ^ IndexKey[(i + 3) % PanjangKey]) + 64) - SecretAdding) % 64;

        DataDecode = VariablePenampungDecode[j++] = ((VariablePenampungPlaintext[0] << 2) | (VariablePenampungPlaintext[1] >> 4)) % 256;

        if (VariablePenampungPlaintext[2] < 64) {

          DataDecode = VariablePenampungDecode[j++] = ((VariablePenampungPlaintext[1] << 4) | (VariablePenampungPlaintext[2] >> 2)) % 256;

          if (VariablePenampungPlaintext[3] < 64) {

            DataDecode = VariablePenampungDecode[j++] = ((VariablePenampungPlaintext[2] << 6) | VariablePenampungPlaintext[3]) % 256;

          }
        }
        Hitung++;
      }
      var HasilDecode = '';
      for (i = 0; i < HitungPanjangPlaintext; i++) {
        HasilDecode += this.chr(VariablePenampungDecode[i]);

      }

      return HasilDecode;
    }

    this.array_sum = function (array) {
      var key;
      var sum = 0;

      if (typeof array !== 'object') {
        return null;
      }

      for (key in array) {
        if (!isNaN(parseFloat(array[key]))) {
          sum += parseFloat(array[key]);
        }
      }

      return sum;
    }

    this.strlen = function (string) {
      return string.length;
    }

    this.str_split = function (string) {
      return string.split('');
    }

    this.ord = function (string) {
      return (string + '').charCodeAt(0);
    }

    this.chr = function (integer) {
      return String.fromCharCode(integer);
    }

    this.strrpos = function (string, index = this.NilaiIndexBaseXorRot47, offset) {
      var i = (string + '').indexOf(index, (offset || 0));
      return i === -1 ? false : i;
    }
  }
};

export default new Enc();