import VueRouter from 'vue-router';
import Dashboard from './components/Dashboard.vue';
import Home from './components/Home.vue';
import Product from './components/Product.vue';
import ProductCreate from './components/products/Create.vue';
import ProductEdit from './components/products/Edit.vue';
import Login from './components/Login.vue';
import userStore from './UserStore' 


let routes = [  
{
  path: '/',
  component: Home,
  meta: { requiresAuth: true} ,
  children: [
  {
    path: '/',
    component: Dashboard,
    name: 'dashboard', 
    meta: { requiresAuth: true} ,
  },
  {
    path: 'products',
    component: Product,
    name: 'product', 
    meta: { requiresAuth: true} ,
  },
  {
    path: 'products/create',
    component: ProductCreate,
    name: 'product-create', 
    meta: { requiresAuth: true} ,
  },
  {
    path: 'products/:id',
    component: ProductEdit,
    name: 'product-edit', 
    meta: { requiresAuth: true} ,
    props: true,
  },
  ],
},
{
  path: '/login',
  component: Login,
  meta: { requiresAuth: false} ,
},


];

const router = new VueRouter({
  routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!userStore.getters.isLoggedIn) {
      next({
        path: '/login',
      });
    } else {
      next();
    }
  } else {
    if (userStore.getters.isLoggedIn) {
     next({
      path: '/',
    });
   }else{
     next();   
   }
 }
})

export default router;