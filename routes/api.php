<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');
Route::post('recover', 'AuthController@recover');
Route::group(['middleware' => ['jwt.auth']], function() {
    Route::get('/users/current', 'UserController@getCurrent');
    Route::get('logout', 'AuthController@logout'); 

    Route::get('products', 'ProductController@index');
    Route::get('products/search/{name}', 'ProductController@searchByName');
    Route::get('products/array', 'ProductController@array_data');
    Route::post('products', 'ProductController@store');
    Route::delete('products/{id}', 'ProductController@destroy');
    Route::post('products/{id}', 'ProductController@update');
    Route::get('products/{id}', 'ProductController@show');


});

Route::post('encrypt', 'TestAlgoritm@encrypt');
Route::post('decrypt', 'TestAlgoritm@decrypt');